import React,{useContext,useState} from 'react';
import {View,Text,StyleSheet,TextInput,Button,Image} from 'react-native';
import { HeaderTitle } from 'react-navigation-stack';


const SuccessScreen = ({navigation}) => {

    return<View>
        <Image style={styles.image} source={{uri:'http://mischacommunications.com/wp-content/uploads/2014/01/MischaQRCode.jpg'}}/>
        <Text style={styles.description}>You are now registered in this event, please get your QR Code scanned for the attendance</Text>
        <Text style={styles.text}>What: Tree planting event</Text>
        <Text style={styles.text}>Where: La mesa dam</Text>
        <Text style={styles.text}>When: April 24 2021:</Text>
        <Text style={styles.text}>Hosted by: NGO NAME</Text>
    </View>
};

const styles = StyleSheet.create({
    image:{
        width:470,
        height:300,
        borderRadius:4,
        marginBottom:5
    },
    text:{
        fontSize:18,
        paddingBottom:5
    },
    description:{
        fontSize:18,
        paddingBottom:20
    }
});

export default SuccessScreen;