import React, {useContext,useEffect} from 'react';
import {View, Text, StyleSheet,FlatList,Button,TouchableOpacity,Image} from 'react-native';
import {Context as EventContext} from '../context/HomeContext';
import {Feather} from '@expo/vector-icons';

const HomeScreen = ({navigation}) => {

const {state,deleteBlogPost,getEvents} = useContext(EventContext);

useEffect (() => {
    getEvents();

    navigation.addListener('didFocus',() => {
        getEvents();
    });
    
},[]);

    return <View style={styles.container}>
        <Text style={styles.title}>Available Events</Text>
        <FlatList 
        data={state}
        keyExtractor={(blogPost) => blogPost.id}
        renderItem={({item}) => {
            return(
            <TouchableOpacity onPress={() => navigation.navigate('Show',{ id: item.id})}>
              <View style={styles.row}>
                  <Image source={{uri: `${item.image}`}}style={styles.image}/>
               </View>
             </TouchableOpacity>
            );
        }}
        />
    </View>
}

HomeScreen.navigationOptions = ({ navigation }) => {
    return {
        headerRight: () => (
            <TouchableOpacity onPress={() => navigation.navigate('Create')}>
              <Feather name="plus" size={30} />
            </TouchableOpacity>
          ),
    };
 
};

const styles = StyleSheet.create({
    row:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingVertical: 20,
        paddingHorizontal:20,
        borderTopWidth:1,
        borderBottomWidth:1,
        borderColor:'gray'
    },
    title:{
        fontSize:18
    },
    icon:{
        fontSize:24
    },
    container:{
        marginLeft:15
    },
    image:{
        width:250,
        height:120,
        borderRadius:4,
        marginBottom:5
    },
    name:{
        fontWeight:'bold',     
    },

});

export default HomeScreen;