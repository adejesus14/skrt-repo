import React, {useContext,useEffect,useState} from 'react';
import {View, Text, StyleSheet,FlatList,Button,TouchableOpacity,TextInput,Image} from 'react-native';
import {MaterialIcons } from '@expo/vector-icons';

const ParticipantScreen = () => {
    return(<View>
       <Image source={{uri: 'https://www.gannett-cdn.com/presto/2020/02/10/PEVC/31e0f88d-2db9-46ee-98d2-85fb99605b70-GettyImages-1135253768.jpg'}}style={styles.image}/>
       <Text style={styles.description}>What: Tree planting event</Text>
       <Text style={styles.description}>Where: La mesa dam</Text>
       <Text style={styles.description}>When: April 24 2021:</Text>
       <Text style={styles.description}>Hosted by: NGO NAME</Text>
        <Text style={styles.row}>Total Participants - 7</Text>
       <Text style={styles.row}>Cong TV - <MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /></Text>
       <Text style={styles.row}>Mimiyuh - <MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /></Text>
       <Text style={styles.row}>Boss Keng - <MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /></Text>
       <Text style={styles.row}>Waldo - <MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /><MaterialIcons name="star-rate" size={20} color="black" /></Text>
       <Text style={styles.row}>Jayzam - <MaterialIcons name="star-rate" size={20} color="black" /></Text>
       <Text style={styles.row}>King Badger - <MaterialIcons name="star-rate" size={20} color="black" /></Text>
       <Text style={styles.row}>Samantha Pettit - <MaterialIcons name="star-rate" size={20} color="black" /></Text>
        <Button title="Back to Home Screen"></Button>
    </View>);
};

const styles = StyleSheet.create({
    center:{textAlign:'center'},
    row:{
        
        flexDirection:'row',
        justifyContent:'space-between',
        paddingVertical: 5,
        paddingHorizontal:5,
        borderTopWidth:1,
        borderBottomWidth:1,
        borderColor:'gray'
    },
    title:{
        fontSize:18
    },
    icon:{
        fontSize:24
    },
    container:{
        marginLeft:15
    },
    image:{
        width:450,
        height:350,
        borderRadius:4,
        marginBottom:5
    },
    name:{
        fontWeight:'bold',     
    },

});

export default ParticipantScreen;