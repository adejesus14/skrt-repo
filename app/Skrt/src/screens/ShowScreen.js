import React,{useContext} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Button} from 'react-native';
import {Context} from '../context/HomeContext';
import {EvilIcons} from '@expo/vector-icons';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
const ShowScreen = ({navigation}) => {
   
    const {state} = useContext(Context);
    const blogPost = state.find((blogPost) => blogPost.id === navigation.getParam('id'));
    console.log(blogPost.image);

    navigation.getParam('id')
    return <View>
   
       <Text style={styles.title}>{blogPost.title}</Text>
       <Image source={{uri: blogPost.image}}style={styles.image}/>
       <MapView style={styles.image}
    initialRegion={{
      latitude: 14.71637,
      longitude:  121.0724,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }}>
    <Marker style={styles.image} coordinate={{ latitude : 14.71637 , longitude : 121.0724, latitudeDelta:0.0922, longitudeDelta:0.0421 }}/></MapView>
       <Text style={styles.description}>{blogPost.description}</Text>
       <Text style={styles.description}>What: Tree planting event</Text>
       <Text style={styles.description}>Where: La mesa dam</Text>
       <Text style={styles.description}>When: April 24 2021:</Text>
       <Text style={styles.description}>Hosted by: NGO NAME</Text>
       <Button title="Register" onPress={() => navigation.navigate('Success')}/>
    </View>
};


ShowScreen.navigationOptions = ({ navigation }) => {
    return {
        headerRight: () => (
            <TouchableOpacity 
            onPress={() => navigation.navigate('Edit',{id: navigation.getParam('id')})}>
               <EvilIcons name="pencil" size={35} />
            </TouchableOpacity>
          ),
    };
 
};


const styles = StyleSheet.create({
    image:{
        width:470,
        height:200,
        borderRadius:4,
        marginBottom:5
    },
    text:{
        textAlign:'center'
    },
    title:{
        fontSize:30,
        paddingBottom:20,
        textAlign:'center'
    },
    description:{
        fontSize:18,
        
       
    }
});

export default ShowScreen;