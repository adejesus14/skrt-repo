import React,{useContext,useState} from 'react';
import {View,Text,StyleSheet,TextInput,Button} from 'react-native';
import {Context} from '../context/HomeContext';
import CreateEventForm from '../components/CreateEventForm';

const CreateScreen = ({navigation}) => {
    const {addEvents} = useContext(Context)
    console.log(Context);

    return <CreateEventForm onSubmit={(title,content,image) => {
        addEvents(title,content,image,() => navigation.navigate('Home'));
    }}/>
};

const styles = StyleSheet.create({
    input:{
        fontSize:18,
        borderWidth:1,
        borderColor:'black',
        marginBottom:15,
        padding:5,
        margin:5
    },
    label:{
        fontSize:20,
        marginBottom:5,
        marginLeft:5
    }
});

export default CreateScreen;