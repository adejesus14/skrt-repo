import React, {useContext,useEffect,useState} from 'react';
import {View, Text, StyleSheet,FlatList,Button,TouchableOpacity,TextInput,Image} from 'react-native';
import LoginForm from '../components/LoginForm';

const LoginScreen = ({navigation}) => {


    return (<View>
        <Image source={{uri: 'https://uspto.report/TM/90083742/mark.png'}}style={{width: 400, height: 400}} />
        <Text style={styles.label}>Enter Username:</Text>
        <TextInput style={styles.input}/>
        <Text style={styles.label}>Enter Password:</Text>
        <TextInput  secureTextEntry={true} style={styles.input}/>
        <Button 
        title="Login"
        onPress={() => navigation.navigate('Home')}/>
        </View>);
}

LoginScreen.defaultProps ={
    initialValues: {
        title:'',
        content:''
    }
};


const styles = StyleSheet.create({
    input:{
        fontSize:18,
        borderWidth:1,
        borderColor:'black',
        marginBottom:15,
        padding:5,
        margin:5
    },
    label:{
        fontSize:20,
        marginBottom:5,
        marginLeft:5
    }
});

export default LoginScreen;