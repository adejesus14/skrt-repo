import React,{useState} from 'react';
import {View,Text,StyleSheet,TextInput,Button} from 'react-native';

const CreateEventForm = ({onSubmit,initialValues}) => {

    const [title,setTitle] = useState(initialValues.title);
    const [content, setContent] = useState(initialValues.content);
    const [image, setImage] = useState(initialValues.image);

    return (<View>
        <Text style={styles.label}>Enter Title:</Text>
        <TextInput value={title} onChangeText={(text) => setTitle(text)} style={styles.input}/>
        <Text style={styles.label}>Enter Content:</Text>
        <TextInput value={content} onChangeText={(content) => setContent(content)} style={styles.input}/>
        <Text style={styles.label}>Enter Image URL:</Text>
        <TextInput value={image} onChangeText={(image) => setImage(image)} style={styles.input}/>
        <Button 
        title="Create New Event"
        onPress={() => onSubmit(title,content,image)}/>
    </View>);
};

CreateEventForm.defaultProps ={
    initialValues: {
        title:'',
        content:'',
        image:''
        }
};

const styles = StyleSheet.create({
    input:{
        fontSize:18,
        borderWidth:1,
        borderColor:'black',
        marginBottom:15,
        padding:5,
        margin:5
    },
    label:{
        fontSize:20,
        marginBottom:5,
        marginLeft:5
    }
});

export default CreateEventForm;