import jsonServer from '../api/jsonServer';
import createDataContext from './createDataContext';

const eventReducer = (state,action) =>{
    switch(action.type){
        case 'get_events':
            return action.payload;
        
        default:
            return state;
    }

};

const getEvents = dispatch => {
    return async () => {
        const response = await jsonServer.get('/events');
        dispatch({type:'get_events',payload:response.data});
    };
};

const addEvents = (dispatch) => {
    return async (title,content,image,callback) =>{
        await jsonServer.post('/events',{title,content,image});
        // dispatch({type : 'add_blogpost',payload:{title,content}});
        if(callback){
            callback();
        }
    };
    
};
const deleteBlogPost = (dispatch) => {
    return (id) =>{
        dispatch({type : 'delete_blogpost',payload: id});
    };
    
};

const editBlogPost = (dispatch) => {
    return (id,title,content,callback) =>{
        dispatch({type : 'edit_blogpost',payload:{id,title,content}});
        if(callback){
            callback();
        }
        
    };
    
};

export const {Context,Provider} = createDataContext(eventReducer,{addEvents,deleteBlogPost,editBlogPost,getEvents},[]);