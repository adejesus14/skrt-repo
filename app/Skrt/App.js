import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import LoginScreen from './src/screens/LoginScreen';
import HomeScreen from './src/screens/HomeScreen';
import CreateScreen from './src/screens/CreateScreen';
import ShowScreen from './src/screens/ShowScreen';
import SuccessScreen from './src/screens/SuccessScreen';
import ParticipantScreen from './src/screens/ParticipantScreen';
import ScanScreen from './src/screens/ScanScreen';
import Notif from './src/screens/Notif';
import { Provider } from './src/context/HomeContext';

const navigator = createStackNavigator({
  Login:LoginScreen,
  Home:HomeScreen,
  Create:CreateScreen,
  Show:ShowScreen,
  Success:SuccessScreen,
  Notif:Notif,
  Participant:ParticipantScreen,
  Scan:ScanScreen
},{
  initialRouteName:'Login ',
  defaultNavigationOptions:{
    title:'Skrt'
  }
});

const App = createAppContainer(navigator);

export default () => {
  return <Provider>
  <App/>
  </Provider> 
};
